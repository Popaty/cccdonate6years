package cccdonate6years

class ReportMoneyTransfer {

    String name;
    String email;
    String amount;
    String methodTransfer;
    Date dateTransfer;


    static constraints = {

    }

    @Override
    public String toString() {
        return "ReportMoneyTransfer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", amount='" + amount + '\'' +
                ", methodTransfer='" + methodTransfer + '\'' +
                ", dateTransfer=" + dateTransfer +
                ", version=" + version +
                '}';
    }
}
